import numpy as np
from random import randint
import matplotlib.pyplot as plt
import os
import matplotlib as mpl

##Randomly kick particles that are in the wrong spot
def randomkicks(numspik,numblock,spikloc,timearray,plusmat):
	for i in range(numspik):
		if(plusmat[i]==0):
			if(randint(0,1)==1):
				newloc=spikloc[i]+randint(-1,1)
	
				if newloc==-1:
					newloc=numblock-1
				if newloc==numblock:
					newloc=0
			
				spikloc[i]=newloc
	
	timearray=np.zeros(numblock)
	for i in range(numblock):
		for j in range(numspik):
			if(spikloc[j]==i):
				timearray[i]+=2**j	
	return timearray,spikloc

def checkvalue(numblock,timearray,template,plusmat):
	for i in range(numblock):
		if(timearray[i]==template[i]):
			goodloc=(np.where(spikloc == i)[0])
			for j in range(len(goodloc)):
				plusmat[goodloc[j]]=1
	return(plusmat)

def saveframe(timearray):
	plt.clf()
	plt.cla()
	plt.close()
	fig, ax = plt.subplots()
	plt.ylim(0,ylims)
	plt.bar(y_pos, timearray, align='center', alpha=0.5)
	plt.xticks(y_pos, y_pos)
	plt.ylabel('Value')
	fig.savefig("frame" + str(kicksteps) + ".png")

path1 ="C:\\Users\\lawso\\Desktop\\Boulder\\Pics"
mpl.rcParams["savefig.directory"] = os.chdir(path1)

numspik=5
numblock=5

timearray=np.zeros(numblock)
template=np.zeros(numblock)
plusmat=np.zeros(numspik)
spikloc=np.zeros(numspik)

for i in range(numblock):
	if(i < numspik):
		timearray[i]=2**(i)
		spikloc[i]=i

if(numspik>numblock):
	for i in range(numblock,numspik):
		timearray[i-numblock]+=2**(i)
		spikloc[i]=i-numblock

ylims=0
for i in range(numspik):
	template[randint(0,numblock-1)]+=(2**i)
	ylims+=2**i
	
	
y_pos=range(numblock)

fig, ax = plt.subplots()
plt.ylim(0,ylims)
plt.bar(y_pos, template, align='center', alpha=0.5)
plt.xticks(y_pos, y_pos)
plt.ylabel('Value')
plt.title('Template')
fig.savefig("template" + ".png")

print(template)
print(timearray)
print(spikloc)

kicksteps=0
done=0
while(done==0):
	checkamount=0
	##Kicking Randomly
	timearray,spikloc=randomkicks(numspik,numblock,spikloc,timearray,plusmat)
	##Checking if the values are right
	plusmat=checkvalue(numblock,timearray,template,plusmat)
	print(timearray)
	kicksteps+=1
	for i in range(numblock):
		if(plusmat[i]==0):
			checkamount+=1
	if(checkamount==0):
		done=1
	saveframe(timearray)
		
print(kicksteps)







	
		


