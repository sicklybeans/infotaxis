import itertools,sys,os

from it_discrete.constants import *
from it_discrete.pdf import Pdf2
from it_discrete.model import Model
from it_discrete.prior import Prior, ProbException
from it_discrete.input import InputGenerator
import it_discrete.loss
import it_discrete.util


class Infotaxis(object):

  def __init__(self, pdf, ordering=True, nearestNeighbor=True, capturing=True, outputEntropy=False, randomMoves=False):
    self.pdf = pdf
    self.outputEntropy = outputEntropy
    self.ordering = ordering
    self.nearestNeighbor = nearestNeighbor
    self.capturing = capturing
    self.randomMoves = randomMoves

    self.inputGen = InputGenerator(self.ordering)
    self.prior = Prior(self.inputGen, self.pdf, capturing=capturing)

  def chooseNextInput(self, lastInput):
    """
    Implements the infotaxis policy. Given the last input, it chooses the next input such that the
    expectation value of the entropy is minimized.
    """
    inputs = []

    # If nearest neighbor is on, only allow moves to adjacent sequences.
    if self.nearestNeighbor:
      inputs = self.inputGen.getAdjacentInputs(lastInput) 
      inputs = list(inputs) + [lastInput]
    else:
      inputs = self.inputGen.getInputs()      

    # Search all potential next inputs for the input with the lowest expected entropy.
    entropyMap = {} # Map from each proposed move to the expected entropy
    minEntropy, minEntropyInput = None, None
    for nextInput in inputs:
      if self.outputEntropy:
        entropy = self.prior.getExpectedOutputEntropy(nextInput)
      else:
        entropy = self.prior.getExpectedEntropy(nextInput)
      entropyMap[nextInput] = entropy

      ## Don't allow system to stay at same position
      #if nextInput == lastInput: continue

      if minEntropy is None or entropy < minEntropy:
        minEntropy, minEntropyInput = entropy, nextInput

    return minEntropyInput, entropyMap

  def run(self, outputFile, xStar, nSteps):
    print('Learning for input %s' % (str(xStar)))
    sys.stdout.flush()

    model = Model(xStar) # Create model for the given target
    self.prior.reset() # Reset prior distribution to be flat

    # Objects to store information about each step of learning
    trialInputs = [] # list of input tried at each time step.
    trialOutputs = [] # list of outputs received at each time step.
    priorMaps = [] # list of maps from (xS1,...xS4) to prior prob
    entropyMaps = [] # list of maps from proposed moves (x1, x2, x3, x4) to expected entropy
    entropy = []
    xStarPosterior = []

    x = self.inputGen.getRandomInput()
    for i in range(nSteps):
      priorMaps.append({xStar: self.prior.priorProb(xStar) for xStar in self.inputGen.getInputs()})

      # 1. Choose new input spike train
      x, entropyMap = self.chooseNextInput(x)
      if self.randomMoves:
        x = self.inputGen.getRandomInput()

      # Compute posterior for the correct input
      xStarPosterior.append(self.prior.priorProb(xStar))

      while True:
        # 2. Get the output for that input
        y = model.getLoss(x)
        y += np.random.normal(scale=LOSS_NOISE)
        yBin = it_discrete.loss.getLossBin(y)

        try:
          # 3. Update the prior distribution.
          self.prior.updatePrior(x,  yBin)
          break
        except it_discrete.prior.ProbException:
          print('improbable event: %s, %s, %d' % (str(xStar), str(x), it_discrete.loss.getLossBin(y)))
          sys.stdout.flush()
          xStarBin = self.prior._starIndicesToIndex[xStar]
          print('Prob(y|x,xstar): %f' % self.prior.pdf._probMap[x][yBin, xStarBin])
          print('Prob(xstar): %f' % self.prior.priorProb(xStar))
          #sys.exit(0)


          continue

      # Save information about this step to output later
      entropyMaps.append(entropyMap)
      trialInputs.append(x)
      trialOutputs.append(y)
      entropy.append(self.prior.getEntropy())
      print('.', end='')
      sys.stdout.flush()
      if self.prior.priorProb(xStar) == 0.0:
        print('Prior on correct spot is 0. Learning failed')
        break

    np.savez(
      outputFile,
      xStar=xStar, nSteps=nSteps, trialInputs=trialInputs, trialOutputs=trialOutputs,
      entropyMaps=entropyMaps, priorMaps=priorMaps, entropy=entropy,
      ordering=self.ordering,
      capturing=self.capturing,
      nearestNeighbor=self.nearestNeighbor,
      outputEntropy=self.outputEntropy,
      randomMoves=self.randomMoves,
      xStarPosterior=xStarPosterior)
    print('Saved results to %s' % outputFile)

if __name__ == '__main__':
  runNum = int(sys.argv[1])
  nSteps = int(sys.argv[2])

  ordering = False
  print('Loading PDF...')
  sys.stdout.flush()
  pdf = Pdf2.loadFromFile(it_discrete.util.getPdfFile(runNum), ordering)
  it = Infotaxis(
    pdf,
    capturing=True,
    nearestNeighbor=True,
    outputEntropy=False,
    randomMoves=False,
    ordering=ordering)

  if len(sys.argv) == 5:
    print('Running simulations')
    sys.stdout.flush()
    numTrials = int(sys.argv[3])
    subdir = sys.argv[4]
    outdir = it_discrete.util.getRunDir(runNum) + subdir
    if not os.path.isdir(outdir):
      os.mkdir(outdir)
    for i in range(numTrials):
      outfile = '%s/infotaxis_%03d.npz' % (outdir, i)
      xStar = it.inputGen.getRandomInput()
      try:
        it.run(outfile, xStar, nSteps)
      except it_discrete.prior.ProbException:
        print('Learn failed due to improbable event')
        continue
  else:
    print('Running simulation')
    sys.stdout.flush()
    outputFile = it_discrete.util.getInfotaxisFile(runNum, suffix='_%d' % nSteps)
    xStar = it.inputGen.getRandomInput()
    it.run(outputFile, xStar, nSteps)

  print('Done')
