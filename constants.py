import itertools
import numpy as np

# Number of seconds each song lasts
T_MAX = 50.0 # seconds

# Number of discrete time intervals
N_TIMES = 8

# Discrete time intervals
TIME_INDICES = list(range(N_TIMES))
TIMES = tuple([(T_MAX/N_TIMES) * (i+0.5) for i in range(N_TIMES)])

def timeIndicesToTimes(timeIndices):
  return [TIMES[i] for i in timeIndices]

def iterateXIndices():
  for i in range(N_TIMES-3):
    for j in range(i+1, N_TIMES-2):
      for k in range(j+1, N_TIMES-1):
        for l in range(k+1, N_TIMES):
          yield (i,j,k,l)
X_INDEX_TUPLES = list(iterateXIndices())


# Numerical integration time step.
DT = 0.1 # seconds

# Time constant for motor output units
TAU = 5.0 # seconds

# Std deviation for noise in spike times (before motor pool mapping)
SPIKE_NOISE_SIGMA = 0.5 # seconds

# Number of motor pools, currently cant be changed
NUM_MOTORS = 2

# Baseline activity for each motor (Supplementary information pg 15 Duffy)
MOTOR_BASE = np.array([60,40])

# Number of RA neurons (i.e. spikes)
NUM_RA_NEURONS = 4

LOSS_NOISE = 0.00

# Default directory to output figures to
FIG_DIR = 'figs'

#Time step for signal perturbation
DT_PERTURB = 5 #seconds
