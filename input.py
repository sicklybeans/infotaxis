import numpy as np
import itertools
from it_discrete.constants import *

class InputGenerator(object):

  def __init__(self, ordering):
    self.ordering = ordering
    if ordering:
      self.inputList = list(InputGenerator._generateOrderedInputs())
    else:
      self.inputList = list(InputGenerator._generateUnorderedInputs())

    self.inputToIndex = {inp: i for i,inp in enumerate(self.inputList)}

  @classmethod
  def _generateOrderedInputs(cls, indices=[]):
    remaining = NUM_RA_NEURONS - len(indices)
    if remaining == 0:
      yield tuple(indices)
    else:
      j1 = indices[-1]+1 if len(indices) != 0 else 0
      for j in range(j1, N_TIMES-remaining+1):
        yield from cls._generateOrderedInputs(indices + [j])

  @classmethod
  def _generateUnorderedInputs(cls):
    return itertools.product(*([TIME_INDICES]*NUM_RA_NEURONS))

  def inputIndex(self, inp):
    return self.inputToIndex[inp]

  def getRandomInput(self):
    return self.inputList[np.random.randint(len(self.inputList))]

  def getInputs(self):
    return self.inputList

  def getAdjacentInputs(self, currentInput):
    """
    Returns list of all legal inputs that are within 1 move from given input.
    """
    moves = [tuple(m) for m in itertools.product(range(NUM_RA_NEURONS), [-1, +1])]
    for index, direc in moves:
      newInput = [currentInput[i]+direc if i==index else currentInput[i] for i in range(len(currentInput))]
      if self.moveIsLegal(newInput):
        yield tuple(newInput)

  def moveIsLegal(self, inputIndices):
    """
    Given a proposed input (x1, x2, x3, x4), returns True is move is valid.
    """
    # Check that all time indices are within bounds
    for x in inputIndices:
      if x < 0 or x >= N_TIMES:
        return False

    # If ordering is on, we demand that x1 < x2 < x3 < x4
    if self.ordering:
      for i in range(len(inputIndices)-1):
        if inputIndices[i] >= inputIndices[i+1]:
          return False

    return True
