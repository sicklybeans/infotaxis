import matplotlib.pyplot as plt
import os
from it_discrete.constants import *

def makeDir(d):
  if not os.path.exists(d):
    os.mkdir(d)
  if not os.path.isdir(d):
    raise Exception('Couldnt make dir: %s' % d)
  return d

def getInfotaxisDir():
  return makeDir(os.path.join(os.environ['HOME'], 'scratch60', 'infotaxis'))

def getRunDir(runNumber):
  return makeDir(os.path.join(getInfotaxisDir(), '%d' % runNumber))

def getDataDir(runNumber):
  return makeDir(os.path.join(getRunDir(runNumber), 'data'))

def getDistDir(runNumber):
  return makeDir(os.path.join(getRunDir(runNumber), 'dists'))

def getJobDir(runNumber):
  return makeDir(os.path.join(getRunDir(runNumber), 'jobs'))

def getPdfFile(runNumber):
  return os.path.join(getRunDir(runNumber), 'pdf.npy')

def getInfotaxisFile(runNumber, suffix=''):
  return os.path.join(getRunDir(runNumber), 'infotaxis%s.npz' % suffix)

def savePlots(xvals, yvalsList, name, xlabel='', ylabel='', outputDir=FIG_DIR):
  """
  Saves a plot for each given yvals named with the index number of data.

  name: Should be a string of the form 'something %d something.png'. The index of the dataset is stored where '%d' is
  """
  for i, yvals in enumerate(yvalsList):
    savePlot(xvals, yvals, name % i, xlabel=xlabel, ylabel=ylabel, outputDir=outputDir)

def savePlot(xvals, yvals, name, xlabel='', ylabel='', outputDir=FIG_DIR):
  plt.clf()
  plt.plot(xvals, yvals)
  if xlabel:
    plt.xlabel(xlabel)
  if ylabel:
    plt.ylabel(ylabel)

  plt.savefig(os.path.join(outputDir, name))

def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 25, fill = 'X'):
  """
  Call in a loop to create terminal progress bar
  @params:
    iteration   - Required  : current iteration (Int)
    total       - Required  : total iterations (Int)
    prefix      - Optional  : prefix string (Str)
    suffix      - Optional  : suffix string (Str)
    decimals    - Optional  : positive number of decimals in percent complete (Int)
    length      - Optional  : character length of bar (Int)
    fill        - Optional  : bar fill character (Str)
  """
  percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
  filledLength = int(length * iteration // total)
  bar = fill * filledLength + '-' * (length - filledLength)
  print ('\r%s |%s| %s%% %s\r' % (prefix, bar, percent, suffix), end='')
  # Print New Line on Complete
  if iteration == total:
    print()
