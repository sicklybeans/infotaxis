import numpy as np
from it_discrete.constants import *


def getMotorSignal(spikeTimes, matrixA, nMotors=NUM_MOTORS, motorBase=MOTOR_BASE, dt=DT, tMax=T_MAX, tau=TAU):
    """
    Generates the numerically integrated values of m1(t), m2(t) from given spike train.
    """
    nSteps = int(tMax/dt)

    # First we have to convert the float valued spikeTimes to the nearest
    # integration time step - eventually we may want to make the "spikes" narrow
    # Gaussians which will require a more sophisticated numerical integration
    # method
    spikeSteps = [int(time/dt) for time in spikeTimes]

    times = np.zeros(nSteps) # list of real valued times
    motors = np.zeros((nMotors, nSteps)) # value of motor pools at those times
    time = 0.0

    for t in range(nSteps):
        times[t] = time

        if t > 0:
            motors[:,t] = (dt/tau) * (motorBase[:] - motors[:,t-1]) + motors[:,t-1]
        else:
            motors[:,t] = motorBase[:]

        # Add RA output to motor pool value
        for j,tStep in enumerate(spikeSteps):
            if tStep == t:
                motors[:,t] += (dt/tau) * matrixA[:,j]

        time += dt

    return motors, times

def getMotorSignalTrials(trials, matrixA, nMotors=NUM_MOTORS, motorBase=MOTOR_BASE, dt=DT, tMax=T_MAX, tau=TAU):
    """
    Generates the numerically integrated values of m1(t), m2(t) from given spike train.
    """
    nTrials = len(trials)
    nSteps = int(tMax/dt)

    spikeSteps = np.floor(trials/dt)

    motors = np.zeros((nTrials, nMotors, nSteps)) # value of motor pools at those times
    time = 0.0
    alpha = dt/tau

    # Initial step
    t = 0
    motors[:,:,t] = motorBase[:]
    activeSpikes = (spikeSteps == t)
    motors[:,0,t] += alpha * (activeSpikes[:,:] @ matrixA[0,:])
    motors[:,1,t] += alpha * (activeSpikes[:,:] @ matrixA[1,:])
    for t in range(1, nSteps):

        motors[:,:,t] = - alpha * (motors[:,:,t-1] - motorBase[:]) + motors[:,:,t-1]
        activeSpikes = (spikeSteps == t)
        motors[:,0,t] += alpha * (activeSpikes[:,:] @ matrixA[0,:])
        motors[:,1,t] += alpha * (activeSpikes[:,:] @ matrixA[1,:])

    return motors
